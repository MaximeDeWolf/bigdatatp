---
title: "Big Data Analytics I - Regression, classification and model selection"
author: "Souhaib Ben Taieb"
output: pdf_document
date: "2 April 2018"
---

```{r, echo = FALSE, message = FALSE, warning = FALSE, warning = FALSE}
knitr::opts_chunk$set(
  message = FALSE,
  warning = FALSE,
  error = FALSE, 
  collapse = TRUE,
  comment = "#",
  fig.height = 4,
  fig.width = 8,
  fig.align = "center",
  cache = FALSE
)
```
# Regression with splines


## Exercise 1

Do some exploratory data analysis on the `Wage` data set (available in the ISLR package).

- Tabulate education and marital status
- Tabulate education and race
- Tabulate marital status race
- Plot marital status as a function of age
- Try other combinations

## Exercise 2

- Fit a spline curve to the relationship between wage and age using two degrees of freedom (`df=2`).
- Experiment with different values of `df` (degrees of freedom) 
- Select one that you think is about right.

## Exercise 3

Now we will test which value of `df` minimizes the MSE on some test data. 

First, we randomly split the `Wage` data set into training and test sets, with 2000 observations in the training data and the remaining 1000 observations in the test data.

```{r}
library(ISLR)
idx <- sample(1:nrow(Wage), size=2000)
train <- Wage[idx,]
test <- Wage[-idx,]
```

- Using a loop, compute the training and test MSE for `df = 1, 2, ..., 20`, and store it in two vectors `trainingMSE` and `testMSE`.
- Plot both `trainingMSE` and `testMSE` as a function of `df`.
- Which value of `df` gives the minimum training MSE?
- Which value of `df` gives the minimum test MSE?
- Plot a vertical line at your "guessed" value of `df`. How close is it to the optimal?
- Do you get the same results if you repeat the exercise on different splits of training and test data? Why?

## Exercise 4
  
- Repeat the previous analysis, but use the full linear model including the other variables in the data set.
- How much better is the test MSE once you include the other predictor variables?
- Check your model by plotting the residuals as a function of each predictor variable. Do you see anything unusual in the residual plots?

# Classification

## Exercice 1

Read and run the code in Sections 4.6.1 to 4.6.6 of ISLR.

## Exercice 2

Exercise 7 in chapter 4.7 of ISLR.

## Exercice 3

Exercise 8 in chapter 4.7 of ISLR.

## Exercice 4

Exercise 10 in chapter 4.7 of ISLR.

## Exercice 5

Download the file ``data_classification.Rdata'' (https://github.com/bsouhaib/ML19/raw/master/data/data_classification.csv) which contains two datasets D1 and D2, each with n = 800 points, $x \in \mathbb{R}^2$ and $y \in \{0, 1\}$.

- (1) Plot the data D1 with the class labels given by y. Run logistic regression, using the glm function in R. What is the training misclassification rate?

- (2) Draw the decision boundary in $\mathbb{R}_2$ of the logistic regression model from (1), on top of your plot from (1). What shape is it? Does this boundary look like it adequately separates the classes?

- (3) Run logistic regression on the predictors $x_1$ and $x_2$, as well as the predictor $x_1^2$. This is analogous to adding a quadratic term to a linear regression. What is the training misclassification rate? Why is this better than the model from (1)?

- (4) Do (1), (2) and (3) for dataset D2. What additional predictors can you pass to logistic regression to improve classification? (Hint: draw a curve between the classes by eye. What shape does this have?)

  