library(ISLR)
library("tidyverse")
library(ggplot2)
library(splines)

# Exercice 1
attach(Wage)

table(education, maritl)
table(education, race)
table(maritl, race)

Wage %>%
  ggplot(aes(x=age, y=maritl)) +
  geom_point()

# Exercice 2
for (df_ in 2:10) {
  fit <- lm(wage~ ns(age, df = df_))
  
  plot(age, wage, main = paste("Df: ", df_))
  points(x = age, y = fitted(fit), col="red")
}
# LOL le meilleur c'est le 2


# Exercice 3
idx <- sample(1:nrow(Wage), size=2000)
train <- Wage[idx,]
test <- Wage[-idx,]

train_MSE <- test_MSE <- numeric(20)
dfs <- 1:20
for (df_ in dfs) {
  fit <- lm(wage~ ns(age, df = df_), data = train)
  t_MSE = mean((fitted(fit) - train$wage)**2)
  train_MSE[df_] <- t_MSE
  
  fit <- lm(wage~ ns(age, df = df_), data = test)
  t_MSE = mean((predict(fit, test) - test$wage)**2)
  test_MSE[df_] <- t_MSE
  
}
plot(x = dfs, y=train_MSE, ylim = c(1400, 1800), col="blue")
points(x= dfs, y = test_MSE, col="red")