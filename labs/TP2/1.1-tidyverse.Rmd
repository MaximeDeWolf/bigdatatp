---
title: "Big Data Analytics I - Lab 1.1"
subtitle: "Introduction to data munging with tidyverse"
author: "Souhaib BEN TAIEB"
date: "14 February 2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
	eval = FALSE,
	message = FALSE,
	warning = FALSE
)
rm(list=ls())
```

## Preliminaries

From Wikipedia: ``The **tidyverse** is a collection of R packages created by Hadley Wickham that "share an underlying design philosophy, grammar, and data structures" of tidy data. The core packages are ggplot2, dplyr, tidyr, readr, purrr, tibble, stringr, and forcats, which provide functionality to model, transform, and visualize data. As of November 2018, the tidyverse package and some of its individual packages make up 5 out of the top 10 most downloaded R packages, and are the subject of multiple books and papers.''

This lab will focus on working with data. Our dataset will contain quality ratings of wine samples and several predictor variables. We will cover loading, munging, and summarising a dataset. The most important package we will be using is `dplyr`. It contains several functions (sometimes referred to as verbs) for manipulating data. Some of the most common verbs and their purpose are

* `select` selects columns
* `mutate` adds new variables
* `filter` filters based on a condition
* `arrange` sorts based on column values
* `group_by` groups data
* `summarise` summarise all values within a group.

A handy reference is Hadley Wickham's R for Data Science available at [r4ds.had.co.nz](http://r4ds.had.co.nz/). Excellent cheat sheets can be downloaded from [www.rstudio.com/resources/cheatsheets/](https://www.rstudio.com/resources/cheatsheets/).



## Exercises

### Exercise 1: Loading data

1. Download the red and white wine quality datasets from the UCI Machine Learning Repository ([archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/](https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/)). 
2. Look at these files in a text editor. 
3. Check the documentation for `read_csv` (from the `readr` package).
4. Select an appropriate function and delimiter to read these files into two separate data frames.

The `readr` functions store data in tibbles. Tibbles are an updated type of data frame and have several advantages over base `R` data frames. One such feature is preserving spaces in the column names. Unfortunately, this can cause issues with other `R` packages so we will convert the white space to underscores. Run the following code to remove all white space from column names.

```{r include=TRUE}
names(red_df) <- str_replace_all(names(red_df), " ", "_")
names(white_df) <- str_replace_all(names(red_df), " ", "_")
```


### Exercise 2: Adding variables

We can use `dplyr`'s `mutate` function to add variables to our data frames.

1. Add a column to your red and white wine data frames indicating the wine colour.
2. Bind these data frames into a single data frame using the `bind_rows` function.
3. Add a Boolean variable that is TRUE if the quality is greater than or equal to 7 (hint: use the `if_else` and `mutate` functions).
4. Add a unique identifier for each wine sample based on row number (hint: use `row_number`). This will be important when converting the table from long to wide formats.


### Exercise 3: Pipes

The pipe operator, `%>%`, takes the output from a previous operation and adds it as the first argument in the next expression. In other words

```{r include = TRUE, eval=FALSE}
mutate(red_df, wine = "Red")
```

is equivalent to

```{r include = TRUE, eval=FALSE}
red_df %>% mutate(wine = "Red")
```

This is very useful when carrying out many operations on a data frame. Chaining verbs together leads to easy to read code. Try incorporating pipes into your previous solution. Can you further tidy the code by combining `mutate` functions?

What if we are interested in more than two categories? Say we wish to try and predict if a wine is good, bad or average. We can do this by creating a data frame with our class definitions and then joining it with our original data frame.

1. Create a new data frame with a quality column (numbers 3, 4, ... 9) and quality category column (Bad, Average, Good).
2. Join this new "quality" data frame with your original wine data frame.


### Exercise 4: Filtering and arranging data

We can arrange the data frame and filter based on conditions using the `arrange` and `filter` functions. Filter for red wines only and arrange in decreasing wine quality.

We can also check for missing values. Run the following code to see which columns have `NA` values. Don't worry about understanding what's going on here - we'll come back to this in a later exercise.

```{r include = TRUE}
wine_df %>% 
  map(~ sum(is.na(.x))) %>% 
  unlist()
```

We see that there are two `NA` values in the `total_sulfur_dioxide` column. Use the filter function to remove these rows (hint: use the `is.na` function and `!` logical operator in your condition).


### Exercise 5: Reshaping data

Our dataset is currently in a wide table format. Variables are spread out across the top of our data frame. Convert the data frame into a long table format using the `gather` function from the `tidyr` package.

You can undo this using the `spread` function to convert the data frame back to a wide format. Spend some time experimenting with reshaping the data frame.


### Exercise 6: Summarising data

Now that we have our data in a long table format we can easily calculate statistics for each variable. Use `dplyr`'s `group_by` function to group the data frame by wine colour, variable and quality category. Calculate the median and standard deviation of each variable using the `summarise` function. The `summarise` function reduces all the values in each group to a single value.

Create a wide table with variables as the key and median as values. Do you notice any obvious differences between good and bad samples?


### Exercise 7: A real world example

The UMass Smart dataset hosts electricity usage and weather data sets. Download the apartment electricity usage dataset at [traces.cs.umass.edu/index.php/Smart/Smart](http://traces.cs.umass.edu/index.php/Smart/Smart). This dataset contains electricity consumption data from 114 single family apartments between 2014 and 2016.

Load the 2015 data into your `R` session and filter for the months January, February and March (hint: the `lubridate` package has several useful functions for working with dates and time). Can you find different classes of energy usage behaviour? For example, do some families use significantly more electricity in the evenings? Do others use more in mornings? How might we describe these classes?
