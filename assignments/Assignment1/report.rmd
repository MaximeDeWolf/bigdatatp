---
title: "Assignment I"
subtitle: "Statistics and exploratory data analysis"
author: "Maxime De Wolf"
date: "17 Mars 2019"
output:
  pdf_document: default
  html_document: default
---

```{r, echo = FALSE, message = FALSE, warning = FALSE, warning = FALSE}
knitr::opts_chunk$set(
  message = FALSE,
  warning = FALSE,
  error = FALSE, 
  collapse = TRUE,
  comment = "#",
  fig.height = 4,
  fig.width = 8,
  fig.align = "center",
  cache = FALSE
)
```
# Question 1
* Trouver l'espérance de $T$.

\begin{align*}
  \mathbb{E}[T] &= \mathbb{E}\left[\sum_{i=1}^n Z_i^2\right] \\                            
                &= \sum_{i=1}^n \mathbb{E}[Z_i^2]                                           \tag{par la linéarité de l'espérance} \\
                &= \sum_{i=1}^n \mathrm{Var}(Z_i) + \mathbb{E}[Z_i]^2
                \tag{car $ \mathbb{E}[Z_i^2] = \mathrm{Var}(Z_i) + \mathbb{E}[Z_i]^2$ : Définition de la variance}\\
                &= \sum_{i=1}^n \sigma^2 + \mu^2                                            \tag{car $Z_i \overset{i.i.d}{\sim} \mathcal{N}(\mu, \sigma^2)$} \\
                &= n (\sigma^2 + \mu^2)\\
\end{align*}


* Trouver la variance de $T$.

\begin{align*}
  \mathrm{Var}(T) &= \mathrm{Var} \left( \sum_{i=1}^n Z_i^2 \right)\\
                  &= \sum_{i=1}^n \mathrm{Var}(Z_i^2)
                  \tag{car les $Z_i$ sont indépendants}\\
                  &= \sum_{i=1}^n \mathbb{E}[Z_i^4] - \mathbb{E}[Z_i^2]^2
                  \tag{car $\mathrm{Var}(X) = \mathbb{E}[X^2] - \mathbb{E}[X]^2$}\\
                  &= \sum_{i=1}^n \mathbb{E}[Z_i^4] - \left( \mathrm{Var}(Z_i) + \mathbb{E}[Z_i]^2 \right)^2
                  \tag{car $ \mathbb{E}[Z_i^2] = \mathrm{Var}(Z_i) + \mathbb{E}[Z_i]^2$ : Définition de la variance}\\
                   &= \sum_{i=1}^n \mathbb{E}[Z_i^4] - ( \sigma^2 + \mu^2)^2
                   \tag{car $Z_i \overset{i.i.d}{\sim} \mathcal{N}(\mu, \sigma^2)$} \\
                   &= \sum_{i=1}^n \mathbb{E}[Z_i^4] - \sigma^4 - 2 \sigma^2 \mu^2 - \mu^4\\
                   &= \sum_{i=1}^n \mu^4 + 6\mu^2 \sigma^2 + 3\sigma^4 - \sigma^4 - 2 \sigma^2 \mu^2 - \mu^4
                   \tag{$\mathbb{E}[X^4] = \mu^4 + 6 \mu^2 \sigma^2 + 3 \sigma^4$ : donné dans l'énoncé}\\
                   &= \sum_{i=1}^n 4\mu^2 \sigma^2 + 2\sigma^4 \\
                   &= 2\sigma^2 \sum_{i=1}^n 2\mu^2 + \sigma^2 \\
                   &= 2n\sigma^2 (2\mu^2 + \sigma^2)
                   \tag{ car $2\mu^2 + \sigma^2$ est une constante}\\
\end{align*}

#Question 2

* Trouver la moyenne de $X$.

\begin{align*}
  \mathbb{E}(X) &= \mathbb{E} \left[ \mathbb{E}(X|C) \right]
                \tag{par définition de l'espérance conditionnelle}\\
                &= \mathbb{E}\left[ X|C=head \right] \times p\left( C=head \right)+\mathbb{E}\left[ X|C=tail \right] \times p\left( C=tail \right)
                \tag{par définition de l'espérance}\\
                &= \frac{\mu_1}{2} +  \frac{\mu_2}{2}
                \tag{car $X \overset{i.i.d}{\sim} \mathcal{N}(\mu_1, \sigma_1^2)$ si la pièce est "face", $X \overset{i.i.d}{\sim} \mathcal{N}(\mu_2, \sigma_2^2)$ si la pièce est "pile"}\\
\end{align*}


* trouver l'écart-type de $X$.

Dans un premier temps, intéressons nous à la variance, c'est-à-dire: $\mathrm{Var}(T)$. Une fois la variance obtenue, nous pouvons calculer l'écart-type en prenant la racine carrée de celle-ci. L'écart-type est donc obtenu comme suit: $\sqrt{\mathrm{Var}(T)}$.

Tout d'abord, calculons $\mathbb{E}\left[ \mathbb{E}(X|C)^2 \right]$ ainsi que $\mathbb{E}\left[ \mathrm{Var}(X|C) \right]$ qui seront nécessaires à la résolution du cacul de la variance.

\begin{align*}
  \mathbb{E}\left[ \mathrm{Var}(X|C) \right] &=  \mathrm{Var}\left( X|C=head \right) \times p(head) + \mathrm{Var}\left( X|C=tail \right) \times p(tail)
                  \tag{par définition de l'espérance}\\ 
                                             &= \frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2}
                  \tag{car $X \overset{i.i.d}{\sim} \mathcal{N}(\mu_1, \sigma_1^2)$ si la pièce est "face", $X \overset{i.i.d}{\sim} \mathcal{N}(\mu_2, \sigma_2^2)$ si la pièce est "pile"}\\
\end{align*}

\begin{align*}
  \mathbb{E}\left[ \mathbb{E}(X|C)^2 \right] &= \mathbb{E}(X|C=head)^2 \times p(C=head) + \mathbb{E}(X|C=tail)^2 \times p(C=tail)
                                             \tag{par définition de l'espérance}\\
                                             &= \frac{\mu_1^2}{2} + \frac{\mu_2^2}{2}
                                             \tag{car $X \overset{i.i.d}{\sim} \mathcal{N}(\mu_1, \sigma_1^2)$ si la pièce est "face", $X \overset{i.i.d}{\sim} \mathcal{N}(\mu_2, \sigma_2^2)$ si la pièce est "pile"}\\
\end{align*}

Concentrons nous maintenant sur le calcul de la variance:

\begin{align*}
  \mathrm{Var}(T) &= \mathbb{E}\left[ \mathrm{Var}(X|C) \right] + \mathrm{Var}\left[ \mathbb{E}(X|C) \right]
                  \tag{par définition de la variance conditionnelle}\\
                  &= \frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2} + \mathrm{Var}\left( \mathbb{E}(X|C) \right)
                  \tag{car calculé ci-dessus}\\
                  &= \frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2} + \mathbb{E}\left[ \mathbb{E}(X|C)^2 \right] - \mathbb{E}\left[ \mathbb{E}(X|C) \right]^2
                  \tag{par définition de la variance}\\
                  &= \frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2} + \mathbb{E}\left[ \mathbb{E}(X|C)^2 \right] - (\frac{\mu_1}{2} + \frac{\mu_2}{2})^2
                  \tag{ car $\mathbb{E}\left[ \mathbb{E}(X|C) \right] = \mathbb{E}(X)$ : calculé ci-dessus}\\
                  &= \frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2} + \frac{\mu_1^2}{2} + \frac{\mu_2^2}{2} - (\frac{\mu_1}{2} + \frac{\mu_2}{2})^2
                  \tag{ car calculé ci-dessus}\\
                  &=  \frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2} + \frac{\mu_1^2}{2} + \frac{\mu_2^2}{2} - (\frac{\mu_1^2}{4}+ \frac{\mu_1 \mu_2}{2} + \frac{\mu_2^2}{4}) \\
                   &=  \frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2} + \frac{\mu_1^2}{2} + \frac{\mu_2^2}{2} - \frac{\mu_1^2}{4} - \frac{\mu_1 \mu_2}{2} - \frac{\mu_2^2}{4} \\
                   &=  \frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2} + \frac{\mu_1^2}{4} + \frac{\mu_2^2}{4} - \frac{\mu_1 \mu_2}{2} \\
                   &=  \frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2} + \left( \frac{\mu_1}{2} - \frac{\mu_2}{2} \right)^2 \\
\end{align*}

Nous sommes donc maintenant capable de calculer l'écart-type en prenant la racine carrée de la variance.

\begin{align*}
  \mathrm{\sigma}_X &= \sqrt{\mathrm{Var}(T)} \\
                    &= \sqrt{\frac{\sigma_1^2}{2} + \frac{\sigma_2^2}{2} + \left( \frac{\mu_1}{2} - \frac{\mu_2}{2} \right)^2}\\
\end{align*}

#Question 3

* Montrons que $\mathbb{E}[Y]=\mu$

\begin{align*}
  \mathbb{E}[Y] &= \mathbb{E}\left[ \frac{1}{n} \sum_{i=1}^n X_i \right] \\
                &= \frac{1}{n} \mathbb{E}\left[ \sum_{i=1}^n X_i \right] 
                \tag{ car $\frac{1}{n}$ est une constante}\\
                &= \frac{1}{n}  \sum_{i=1}^n \mathbb{E} [X_i] 
                \tag{ par la linéarité de l'espérance}\\
                &= \frac{1}{n}  \sum_{i=1}^n \mu 
                \tag{ car $\mathbb{E} [X_i] = \mu$: voir énoncé}\\
                &= \frac{1}{n} n \mu 
                \tag{ car $\mu$ est une constante}\\
                &= \mu \\
\end{align*}

* Montrons que $\mathrm{Var}(Y)=\sigma$

\begin{align*}
  \mathrm{Var}(Y) &= \mathrm{Var} \left( \frac{1}{n} \sum_{i=1}^n X_i \right) \\
                  &= \frac{1}{n^2} \mathrm{Var} \left( \sum_{i=1}^n X_i \right)
                  \tag{car $\mathrm{Var}(aX+b) = a^2 \mathrm{Var}(X) $ si a est une constante}\\
                  &= \frac{1}{n^2} \sum_{i=1}^n \mathrm{Var}(X_i)
                  \tag{car tous les $X_i$ sont indépendants}\\
                  &= \frac{1}{n^2} \sum_{i=1}^n \sigma^2
                  \tag{car $\mathrm{Var}(X_i) = \sigma^2$: voir énoncé}\\
                  &= \frac{1}{n^2} n \sigma^2
                  \tag{car $\sigma^2$ est une constante}\\
                  &= \frac{\sigma^2}{n}
\end{align*}

#Question 4
* Soit $\mu$ = 1, considérons quatre scénarios où $n$ = 10, 1000 et $\sigma$ = 1, 5.
* Pour chaque scénario, répétons la procédure suivante 10.000 fois:
1. Générer $n$ *i.i.d.* réalisations $X_1, X_2, ..., X_n$ où $\mathbb{E}[X_i] = \mu$ et $\mathrm{Var}(X_i) = \sigma^2$.
2. Calculer $Y = \frac{1}{n} \sum_{i=1}^n X_i$.
* Pour chaque scénario, calculer la moyenne (empirique) et la variance de $Y$ , et discuter comme une fonction de $n$ et $\sigma$ en utilisant la réponse de la question 3.
* Pour chaque scénario, dessiner un histogramme des 10.000 valeurs de Y.
```{r}
library(ggplot2)
library(tidyverse)


compute_theorical_mean <- function(mu){
  return(mu)
}

compute_theorical_var <- function(sigma, n){
  return(sigma*sigma/n)
}

apply_scenario <- function(mu, sigma, n)
{
  Y_list <- c()
  for( i in 1:10000){
    realisations <- rnorm(n, mean=mu, sd=sigma)
    Y <- mean(realisations)
    Y_list <- c(Y_list, Y)
  }
  
  Y_mean <- mean(Y_list)
  Y_var <- var(Y_list)
  
  theorical_mean <- compute_theorical_mean(mu)
  theorical_variance <- compute_theorical_var(sigma, n)
  
  print(sprintf("Scenario: mu=%i, sigma=%i and n=%i", mu, sigma, n))
  print(sprintf("theorical mean VS empirical mean: %f VS %f",theorical_mean , Y_mean))
  print(sprintf("theorical variance VS empirical variance: %f VS %f", theorical_variance, Y_var))
  
  # Plotting part
  
  Y_list <- data.frame(Y_list)
  
  print(
   Y_list %>%
     ggplot( aes(Y_list) ) +
     geom_histogram() +
     geom_vline(xintercept = theorical_mean, linetype = "dashed", colour = "red")
  )
}

apply_scenario(1, 1, 10)
apply_scenario(1, 1, 1000)
apply_scenario(1, 5, 10)
apply_scenario(1, 5, 1000)

```
Nous pouvons voir que les variances et les moyennes collent assez bien avec les valeurs théoriques.

#Question 5

* Ecrire la vraisemblance comme une fonction du jeu de données $X_1, X_2, ...,X_n$ et du paramètre inconnu $p$.

Nous allons d'abord calculer la vraisemblance: $\mathrm{L}(p)$.

\begin{align*}
  \mathrm{L}(p) &= \prod_{i=1}^n p \times (1-p)^{X_i -1}
                \tag{ par définition de la vraisemblance et parce qu'on fait l'hypothèse que nos données suivent une loi géométrique}\\
                &= p^n  \prod_{i=1}^n (1-p)^{X_i -1}
                \tag{car $p$ est une constante}\\
\end{align*}

Ensuite nous calculons le log-vraisemblance qui sera plus facile à utiliser dans la deuxième partie de la question. Comme son nom l'indique, le log-vraisemblance $\mathrm{l}(p)$ est définit comme suit: $\mathrm{l}(p) = log(\mathrm{L}(p))$

\begin{align*}
  \mathrm{l}(p) &= log(\mathrm{L}(p)) \\
                &= log \left( p^n  \prod_{i=1}^n (1-p)^{X_i -1} \right)\\
                &= log(p^n) + log \left( \prod_{i=1}^n (1-p)^{X_i -1} \right)
                \tag{car $log(a \times b) = log(a) + log(b)$}\\
                &= log(p^n) + \sum_{i=1}^n log \left( (1-p)^{X_i -1} \right)
                \tag{car $log(a \times b) = log(a) + log(b)$}\\
                &= n \times log(p) +  \sum_{i=1}^n (X_i - 1) log (1-p)
                \tag{par définition des logarithmes}\\
                &= n \times log(p) + log (1-p) \sum_{i=1}^n X_i - 1
                \tag{car $log (1-p)$ est une constante}\\
                &= n \times log(p) + log (1-p) \left( \sum_{i=1}^n X_i - \sum_{i=1}^n 1 \right)\\
                &= n \times log(p) + log (1-p) \left( n\overline{X} - \sum_{i=1}^n 1 \right)
                \tag{ avec $\overline{X}$ la moyenne empirique des $X_i$}\\
                &= n \times log(p) + log (1-p) \left( n\overline{X} - n \right)\\
                &= n \times log(p) + n \times log (1-p) (\overline{X} - 1)\\
\end{align*}

* Calculer le *MLE* de $p$.

Afin de calculer le *MLE* de $p$, nous devons tout d'abord trouver la dérivée de $\mathrm{l}(p)$: $\mathrm{l}(p)'$. Ensuite, nous devons calculer la valeur de $p$ pour laquelle  $\mathrm{l}(p)'$ est nulle. Cette valeur, représente donc un maximum ou un minimum. Comme nous ne nous intéressons qu'au maximum pour trouver le *MLE*, nous vérifierons qu'il s'agit bien d'un sommet en calculant la dirivée seconde du log-vraisemblance en la valeur de $p$ trouvée plus tôt. Si cette dérivé est négative, alors il s'agit bien d'un sommet. La valeur de $p$ trouvée plus tôt est donc la valeur qui maximise les chances d'obtenir notre échantillons de données à partir d'une loi $Geom(p)$.


Calculons d'abord la dérivée première du log-vraisemblance.
\begin{align*}
  \mathrm{l}(p)' &= \left( n \times log(p) + log(1-p) (n \overline{X} - n) \right)'\\
                &= \left( n \times log(p) \right)' +  \left( log(1-p) (n \overline{X} - n) \right)'
                \tag{car $(f(x) + g(x))' = f(x)' + g(x)'$}\\
                 &=  n \times log(p)' + (n \overline{X} - n) \times log(1-p)'
                 \tag{car $(c \times f(x))' = c \times f(x)'$ si $c$ est une constante}\\
                 &=  n \frac{1}{p} + (n \overline{X} - n) \frac{-1}{1-p}
                 \tag{car $(log(f(x)))' = \frac{f(x)'}{f(x)}$}\\
\end{align*}

Maintenant calculons la valeur de $p$ qui annule cette dérivée.

\begin{align*}
  \mathrm{l}(p)' &= 0\\
  \frac{n}{p} + \frac{-n \overline{X} + n}{1-p} &= 0\\
  \frac{n}{p}  &= \frac{n \overline{X} - n}{1-p}\\
  \frac{1}{p}  &= \frac{\overline{X} - 1}{1-p}\\
  1-p &= p\overline{X} - p\\
  1 &= p\overline{X} \\
  p &= \frac{1}{\overline{X}}\\
\end{align*}

Nous savons maintenant que $l \left(\frac{1}{\overline{X}} \right)$ est soit un maximum, soit un minimum. Interressons nous à la valeur de la dérivée seconde du log-vraisemblance en $\frac{1}{\overline{X}}$. Si cette valeur est négative, alors, $l \left(\frac{1}{\overline{X}} \right)$ est bien un sommet ce qui veut dire que $\frac{1}{\overline{X}}$ est bien le *MLE* de $p$. Calculons d'abord la dérivée seconde de $\mathrm{l}(p)$.

\begin{align*}
  \mathrm{l}(p)'' &= \left( \frac{n}{p} + \frac{-n \overline{X} + n}{1-p} \right)' \\
                  &= \left( \frac{n}{p} \right)' + \left( \frac{-n \overline{X} + n}{1-p} \right)'
                  \tag{car $(f(x) + g(x))' = f(x)' + g(x)'$}\\
                  &= n \left( \frac{1}{p} \right)' -n (\overline{X} - 1) \left( \frac{1 }{1-p} \right)'
                  \tag{car $(c \times f(x))' = c \times f(x)'$ si $c$ est une constante}\\
                  &= n \frac{-1}{p^2} - n (\overline{X} - 1) \frac{1}{(1-p)^2}
                  \tag{car $\left( \frac{1}{f(x)} \right)'= \frac{-f(x)'}{f(x)^2}$}\\
                  &= \frac{-n}{p^2} -  \frac{n (\overline{X} - 1)}{1 - 2p + p^2}\\
\end{align*}

Calculons maintenant $\mathrm{l}(\frac{1}{\overline{X}})''$ pour s'assurer que le résultat est bien inférieur à zéro et donc prouver que le *MLE* de $p$ est bien $\frac{1}{\overline{X}}$.


\begin{align*}
  \mathrm{l}(\frac{1}{\overline{X}})'' &= \frac{-n}{\left( \frac{1}{\overline{X}} \right)^2} - \frac{n (\overline{X} - 1)}{1 - \frac{2}{\overline{X}} + \left( \frac{1}{\overline{X}} \right)^2 }\\
                                       &= \frac{-n}{\frac{1}{\overline{X^2}}} - \frac{n (\overline{X} - 1)}{1 - \frac{2}{\overline{X}} + \frac{1}{\overline{X}^2} }\\
                                       &=  -n\overline{X}^2 - \frac{n (\overline{X} - 1)}{\frac{\overline{X}^2}{\overline{X}^2} - \frac{2\overline{X}}{\overline{X}^2} + \frac{1}{\overline{X^2}} }\\
                                       &=  -n\overline{X}^2 - \frac{n \overline{X}^2 (\overline{X} - 1)}{\overline{X}^2 - 2\overline{X} + 1 }\\
                                       &=  -n\overline{X}^2 - \frac{n \overline{X}^2 (\overline{X} - 1)}{(\overline{X}- 1)^2 }\\
                                       &=  -n\overline{X}^2 - \frac{n \overline{X}^2}{(\overline{X}- 1)}\\
                                       &= \frac{-n\overline{X}^2 (\overline{X}- 1)}{(\overline{X}- 1)} - \frac{n \overline{X}^2}{(\overline{X}- 1)}\\
                                       &= \frac{-n\overline{X}^2 (\overline{X}- 1)-n \overline{X}^2}{(\overline{X}- 1)}\\
                                       &= \frac{-n\overline{X}^2 \left((\overline{X}- 1) +1 \right)}{(\overline{X}- 1)}\\
                                       &= \frac{-n\overline{X}^3}{\overline{X}- 1}\\
\end{align*}

Analysons le signe de $\frac{-n\overline{X}^3}{\overline{X}- 1}$. D'abord, $\overline{X}- 1$ est supérieur ou égal à zéro car $X_1, X_2, ..., X_n \in \{1, 2, 3, ...\}$. Ensuite, $-n\overline{X}^3$ est négatif car $\overline{X}- 1 \leq 0$ et $n > 0$. On peut donc en conclure que $\frac{-n\overline{X}^3}{\overline{X}- 1}$ est bien négatif. Cela implique que  $l \left(\frac{1}{\overline{X}} \right)$ est un maximum et donc que $\frac{1}{\overline{X}}$ est bien le $MLE$ de $p$.

#Question 6
Ecrivez du code pour répondre aux questions suivantes:

**1. Quels sont les attributs les moins désirables chez un partenaire masculin ? Est-ce que cela diffère pour les partenaires féminins?**

Cette question est un peu floue car ces attributs ont été mesurés à différents moments de l'expérience et n'ont pas toujours été mesurés de la même façon pour toutes les *vagues*. Nous nous sommes donc concentrés sur les données récoltées juste avant d'entamer l'expérience. Ces attributs représentent donc l'état d'esprit des candidats *a priori*. 

```{r}
library("tidyverse")


dates <-read.csv("speed-dating-data.csv", sep=",", dec = ".")

dates %>%
  select(iid, gender, attr1_1, sinc1_1, intel1_1, fun1_1, amb1_1, shar1_1) %>%
  unique() %>%
  group_by(gender) %>%
  summarise_at(vars(attr1_1, sinc1_1, intel1_1, fun1_1, amb1_1, shar1_1), mean, na.rm=TRUE) 

```
Dans ce tableau, les femmes sont représentées par 0 et les hommes par 1. Ce tableau expose la moyenne du score obtenu pour chaque attribut, affichée séparemment pour les hommes et les femmes. On peut donc s'en servir pour répondre facilement à la question.

L'attribut le moins recherché par un **homme** chez un partenaire est l'_ambition_ suivi de près par l'_importance de partager des centres d'intérêts_. Il en va de même pour les femmes même si cette tendance est moins marquée. En effet, chez les **femmes**, le _partage de centes d'intérêts_ obtient un score quasimment identique à l'_ambition_. On remarque également que ces deux attributs obtiennent une note plus élévée que chez les hommes même s'ils partagent la même place au classement.

**2. Quelle importance donnent les gens à l'attractivité dans une sélection de potentiel companion et quel est son véritable impact ?**

Pour répondre à cette question, nous allons comparer le score alloué à l'attractivité par chaque participant à deux étapes différentes. Nous allons comparer ses valeurs avant la participation à l'expérience et une fois qu'un participant à *matché* un partenaire. 

```{r}
dates %>%
  select(iid, attr1_1, attr7_2) %>%
  unique() %>%
  summarise_at(vars(attr1_1, attr7_2), mean, na.rm=TRUE)
```
Ce tableau montre l'importance donnée en moyenne à l'attractivité avant l'expérience -*a priori*- ainsi que l'importance réelle qu'elle a eu sur le choix d'un partenaire. Nous pouvons voir que l'attractivité avait dès le début un score assez élévé. Nous voyons également que cet attribut a eu encore plus d'importance pour le choix d'un partenaire que ce que pensaient les participants.


**3. Est-ce que des centres d'intérets partagés sont plus important qu'une origine ethnique partagée ?**

Pour répondre à cette question, nous allons comparer les valeurs données à l'importance de partager des centres d'intérêts et à l'importance de partager la même origine ethnique. Pour cette question, nous utilisons une version normalisée de l'attribut d'importance de partager les centres d'intérêts. Ainsi, ces deux attributs partagent le même système de notation sur 10 et peuvent être comparé sans difficulté.

```{r}
dates %>%
  select(iid, shar, imprace) %>%
  unique() %>%
  summarise_at(vars(shar, imprace), mean, na.rm=TRUE)
```
Ce tableau montre en moyenne la note sur 10 attribuée à l'importance de partager des centres d'intérêts et à l'importance de partager la même origine ethnique. Nous voyons qu'en moyenne, l'importance de partager des centres d'intérêts est significativement plus important que de partager la même origine ethnique.

**4. Est-ce que les gens peuvent prédire précisemment leur valeur sur le _"dating market"_ ?**

Pour répondre à cette question, nous allons, pour chaque observations, déterminer l'erreur absolue entre le score que s'octroie un participant à lui même et le score que son rendez-vous lui donne. Ensuite, nous calculerons l'erreur moyenne pour chaque attribut.

```{r}
absolute_error <- function(x, y){
  return(abs(x - y))
}

dates %>%
  select(iid, attr3_1, sinc3_1, intel3_1, fun3_1, amb3_1, attr_o, sinc_o, intel_o, fun_o, amb_o) %>%
  mutate(attr_err=absolute_error(attr3_1, attr_o)) %>%
  mutate(sinc_err=absolute_error(sinc3_1, sinc_o)) %>%
  mutate(intel_err=absolute_error(intel3_1, intel_o)) %>%
  mutate(fun_err=absolute_error(fun3_1, fun_o)) %>%
  mutate(amb_err=absolute_error(amb3_1, amb_o)) %>%
  summarise_at(vars( attr_err, sinc_err, intel_err, fun_err, amb_err), mean, na.rm=TRUE)
```
Nous pouvons remarquer qu'en moyenne les gens ont du mal à estimer leur valeur sur le _"dating market"_. En effet, nous voyons que peu importe l'attribut, l'erreur se situe entre 1.6 et 2 ce qui est assez élévé car l'erreur maximal est de 9. Nous remarquons également que l'ambition et la faculté à être marrant sont les deux attributs les moins bien estimés.

**5. En terme d'obtention de second rendez-vous, est-il plus intéressant d'être le premier rendez-vous de quelqu'un ou le dernier de la soirée ?**

Pour résoudre cette question, nous allons compter le nombre de "*match*" à chaque étape du "*speed dating*". Nous considérons qu'une étape se passe à chaque fois que les participants change de rendez-vous.

```{r}
select(dates, order, dec) %>%
  filter(dec==1) %>%
  group_by(order) %>%
  count()
```
Ce tableau affiche le nombre de "*match*" à chaque étape du "*speed dating*". Dès lors, nous voyons clairement qu'il y a beaucoup plus de "*match*" à l'étape 1 qu'à l'étape 20. 

Néanmoins, cette analyse est à prendre avec des pincettes car toutes les *vagues* n'ont pas le même nombre d'étapes. Effectivement, il y a qu'une *vague* qui était composée de 20 étapes. Il est donc logique que moins de participants ont "*matchés*" lors de cette étape. 
Le nombre minimum d'étapes pour une *vague* est 5. Nous pouvons donc analyser les 5 premières étapes sans tenir compte des différences entre *vagues*. Si nous faisons cela, nous voyons effectivement qu'il y a plus de "*matchs*" à l'étape 1 qu'à l'étape 5. Cependant, cette différence est trop petite pour dire qu'elle est significative.

Il faudrait que le nombre d'étapes par vagues soit plus uniforme pour pouvoir faire une analyse pertinente de cette tendance.

**6. Ecrivez deux autres observations intéressantes à propos du jeu de données.**

1. Les hommes et les femmes commettent-ils les mêmes erreurs en terme d'auto-évaluation de leur valeur sur le _"dating market"_ ?

Pour répondre à cette question nous allons adopter la même approche que pour résoudre la sous-question 4. Effectivement, nous allons calculer l'erreur absolue entre l'auto-évaluation d'un participant par rapport à l'évaluation faite par son rendez-vous. Ensuite, nous allons calculer cette erreur en moyenne en fonction du genre du participant.

```{r}
dates %>%
  select(iid, gender, attr3_1, sinc3_1, intel3_1, fun3_1, amb3_1, attr_o, sinc_o, intel_o, fun_o, amb_o) %>%
  mutate(attr_err=absolute_error(attr3_1, attr_o)) %>%
  mutate(sinc_err=absolute_error(sinc3_1, sinc_o)) %>%
  mutate(intel_err=absolute_error(intel3_1, intel_o)) %>%
  mutate(fun_err=absolute_error(fun3_1, fun_o)) %>%
  mutate(amb_err=absolute_error(amb3_1, amb_o)) %>%
  group_by(gender) %>%
  summarise_at(vars( attr_err, sinc_err, intel_err, fun_err, amb_err), mean, na.rm=TRUE)
```

Ce tableau montre les erreurs absolues commises en moyenne lors de l'auto-évaluation des participants séparément pour les hommes et les femmes. Ces valeurs représentent une erreur absolue sur une cotation allant de 1 à 10. Nous pouvons donc dire qu'il n'existe pas de différence significative entre les hommes et les femmes pour ce qui est d'erreurs d'auto-évaluation de leur valeur sur le _"dating market"_.

En effet, la différence la plus marquée se trouve au niveau de l'attractivité mais ne vaut que 0.1 .

2. Y-a-t-il plus de participants qui "*matchent*"en fonction du choix de leurs études ?

Pour résoudre cette question, nous allons, pour chaque choix d'étude, compter le nombre de "*matchs*" des participants. Nous verrons ainsi si il y a une différe,nce significative suivant l'étude.

```{r}

dates %>%
  select(iid, field_cd, dec) %>%
  filter(dec==1) %>%
  unique() %>%
  group_by(field_cd) %>%
  count()
```

Ce tableau compte le nombre de *"matchs"* obtenues suivant chaque choix d'étude. Il y a effectivement de grande diffirence suivant ce choix. Nous voyons qu'il y a énormément de "*match*" pour les participants suivants les études 8: **Business, Economie et Finance**. On peut donc conclure qu'un participant suivant ces études a plus de chance de "*matcher*".

Encore une fois, cette conclusion est à prendre avec des pincettes car nous n'avons aucune idée de la distribution des choix d'études des participants. En effet, si une grande partie des participants suivent les études 8, il est alors normal de voir ce type de participant "*matcher*" plus souvent.